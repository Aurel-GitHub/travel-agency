<?php

namespace App\Tests;

use App\Entity\Country;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testDemo(): void
    {
        $country = new Country;
        $country->setName('France');

        $this->assertTrue($country->getName() === 'France');
    }
}
